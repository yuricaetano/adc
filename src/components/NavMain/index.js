import React, { Component } from 'react';
import { GoMail } from 'react-icons/go';
import { FaWhatsapp } from 'react-icons/fa';
import { Container,Title, Nav } from './styles';

import Social from '../SocialMedia/index';
import { Link } from 'react-router-dom';

export default class NavMain extends Component {
  render() {
    return (
       <Container>
           <Title>
           <Title><GoMail size={35} /> aprendadireitocompleto@gmail.com</Title>
           <Title id="whats"><FaWhatsapp size={28} /> (13)99756.9515</Title>
           </Title>
           <Social />
           <Nav>
                <button>Login</button>
                <Link to="register">Registrar</Link>
                <button id="assinar">QUERO ASSINAR</button>
           </Nav>
           
       </Container>
    );
  }
}