import styled from "styled-components";

export const SocialMedia = styled.ul`
    display: flex;
    align-items: center;
    margin-top: 5px;
    a {
        color: #8e8e8e;
        border-radius: 50%;
        text-decoration: none;
    }
`;

export const Ex = styled.a`
    display: flex;
    margin-bottom: 5px;
    align-items: center;
    justify-content: center;
    margin-right: 2px;
    height: 30px;
    width: 30px;
    background-color: #8e8e8e;
    svg{
        color: #2a2a2a
    }

`;