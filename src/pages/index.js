import React from 'react';
import { Redirect } from 'react-router-dom'

// import { Container } from './styles';

function pages() {
  return <Redirect to='home' />;
}

export default pages;